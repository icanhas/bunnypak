// Mists are very light fogs, suitable for when the viewer will be inside a volume all the time.
// Scatters are very, very light fogs.

textures/weather/mist_ltblue
{
	qer_editorimage textures/common/fog
	qer_trans 0.25

	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm fog
	surfaceparm nolightmap

	qer_nocarve
	fogparms ( 0.45 0.5 0.6 ) 21000
}

textures/weather/scatter_ltblue
{
	qer_editorimage textures/common/fog
	qer_trans 0.25

	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm fog
	surfaceparm nolightmap

	qer_nocarve
	fogparms ( 0.45 0.5 0.6 ) 34000
}

textures/weather/rain
{
	cull none
	qer_editorimage textures/common/rain
	qer_trans 0.25
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap
	deformvertexes wave 194 triangle 1 50 0 .4
	qer_nocarve
	{
		map textures/sprites/rainfall
		blendfunc blend
		tcmod scroll 0.5 -5
		tcmod scroll -0.1 0
		tcmod turb 0.1 0.25 0 -0.1
		tcmod transform 1.1 0 0 1.05 7 7
	}
	{
		map textures/sprites/rainfall
		blendfunc blend
		tcmod turb 0.1 0.1 0 -0.06
		tcmod scroll 0.01 -6.3
		tcmod scroll 0.032 0
		tcmod transform 1 0 0 1 77 77
	}
}

textures/weather/rainlight
{
	cull none
	qer_editorimage textures/common/rain
	qer_trans 0.25
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap
	surfaceparm noimpact
	deformvertexes wave 194 triangle 1 50 0 .4
	qer_nocarve
	{
		map textures/sprites/rainfalllight
		blendfunc blend
		tcmod scroll 0.5 -4
		tcmod scroll -0.1 0
		tcmod turb 0.1 0.25 0 -0.1
		tcmod transform 1.1 0 0 1.05 7 7
	}
}

textures/weather/snow
{
	cull none
	qer_editorimage textures/common/snow
	qer_trans 0.25
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm nolightmap
	surfaceparm noimpact
	deformvertexes bulge 60 20 2
	qer_nocarve
	{
		map textures/sprites/snowfall
		blendfunc blend
		tcmod scroll 0.2 -0.4
		tcmod turb 0.25 0.25 0 -0.1
		tcmod transform 3 0 0 3 7 7
	}
}
