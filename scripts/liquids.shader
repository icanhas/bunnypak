textures/liquids/pool1
{
	qer_editorimage textures/liquids/pool1ripple1
	qer_trans .5
	q3map_globaltexture
	surfaceparm trans
	surfaceparm nonsolid
	surfaceparm water	
	cull disable
	deformVertexes wave 64 sin .8 .25 0 .5	
	{ 
		map textures/liquids/pool1ripple2
		blendFunc GL_dst_color GL_one
		rgbgen identity
		tcmod scale .5 .5
		tcmod turb 1 .1 0 0.21
	}	
	{ 
		map textures/liquids/pool1ripple1
		blendFunc GL_dst_color GL_one
		tcmod scale -.5 -.5
		tcmod turb 1 .1 1 0.2
	}
	{
		map $lightmap
		blendFunc GL_dst_color GL_zero
		rgbgen identity		
	}
}
