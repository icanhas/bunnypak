//
// items
//

models/items/token
{
	cull none
	surfaceparm trans
	surfaceparm nolightmap
	{
		map models/items/token
		rgbgen lightingdiffuse
		depthwrite
		blendfunc blend
		alphafunc GE128
	}
}

models/items/key_ruby
{
	cull none
	{
		map textures/effects/envmap
                tcgen environment
		rgbgen lightingdiffuse
		alphagen wave square 0 .2 0 0
		blendfunc blend
		depthwrite
	}
	{
		map models/items/key_ruby
		blendfunc blend
		rgbgen lightingdiffuse
		depthwrite
	}
}

models/items/key_jade
{
	cull none
	{
		map textures/effects/envmap
                tcgen environment
		rgbgen lightingdiffuse
		alphagen wave square 0 .2 0 0
		blendfunc blend
		depthwrite
	}
	{
		map models/items/key_jade
		blendfunc blend
		alphagen wave square 0 .95 0 0
		depthwrite
		rgbgen lightingdiffuse
	}
}

models/items/key_sapphire
{
	cull none
	{
		map textures/effects/envmap
                tcgen environment
		rgbgen lightingdiffuse
		alphagen wave square 0 .5 0 0
		blendfunc blend
		depthwrite
	}
	{
		map models/items/key_sapphire
		blendfunc blend
		rgbgen lightingdiffuse
		depthwrite
	}
}

//
// mapobjects
//

// checkpoint
models/mapobjects/ckpoint/ckpointhalo
{
	cull none
	surfaceparm trans
	surfaceparm detail
	surfaceparm nonsolid
	{
		map models/mapobjects/ckpoint/ckpointhalo
		rgbgen identity
		tcmod rotate 10
		tcmod stretch sin 1.2 .1 0 .33
		tcmod turb 0 0.02 0 0.05
		blendfunc blend
	}
}

models/mapobjects/ckpoint/sparkle
{
	cull none
	surfaceparm trans
	surfaceparm detail
	surfaceparm nonsolid
	{
		map models/mapobjects/ckpoint/sparkle
		rgbgen identity
		tcmod rotate 70
		tcmod stretch sin 1.2 .1 0 .33
		tcmod turb 0 0.02 0 0.05
		blendfunc blend
	}
}

// fences
models/mapobjects/fences/fence1
{
	cull none
	surfaceparm depthwrite
	{
		map models/mapobjects/fences/fence1
		rgbgen vertex
	}
}

models/mapobjects/fences/fence3
{
	cull none
	surfaceparm trans
	surfaceparm depthwrite
	{
		map models/mapobjects/fences/fence3
		alphafunc GE128
		blendfunc blend
		depthwrite
		rgbgen vertex
	}
}

// flora
models/mapobjects/flora/jungleplant1
{
	cull none
	surfaceparm depthwrite
	surfaceparm alphashadow
	{ 
		map models/mapobjects/flora/jungleplant1
		rgbgen vertex
	} 
}

models/mapobjects/flora/jungleplant2
{
	cull none
	surfaceparm depthwrite
	surfaceparm alphashadow
	{ 
		map models/mapobjects/flora/jungleplant2
		rgbgen vertex
	} 
}

models/mapobjects/flora/fern1
{
	cull none
	surfaceparm depthwrite
	surfaceparm alphashadow
	{
		map models/mapobjects/flora/fern1
		blendfunc blend
		rgbgen vertex
		depthwrite
	}
}

models/mapobjects/flora/pumpkin2
{
	q3map_surfacelight 600
	q3map_lightimage models/mapobjects/flora/pumpkin2face
	{
		map models/mapobjects/flora/pumpkin
		rgbgen vertex
	}
	{
		map models/mapobjects/flora/pumpkin2face
		rgbgen identity
		blendfunc add
		alphafunc GE128
	}
}

models/mapobjects/flora/tree1
{
	cull none
	surfaceparm depthwrite
	{
		map models/mapobjects/flora/tree1
		blendfunc blend
		rgbgen vertex
		depthwrite
		alphafunc GE128
	}
}

models/mapobjects/flora/nettleleaf
{
	cull none
	surfaceparm alphashadow
	{
		clampmap models/mapobjects/flora/nettleleaf
		blendfunc blend
		alphafunc GE128
		depthwrite
		rgbgen vertex
	}
}

// lights
models/mapobjects/lights/lantern1
{
	cull none
	q3map_surfacelight 57000
	q3map_lightimage models/mapobjects/lights/flame
	surfaceparm nonsolid
	surfaceparm trans
	surfaceparm lightfilter
	surfaceparm alphashadow
	{
		map models/mapobjects/lights/lantern1
		alphafunc GE128
		depthwrite
		blendfunc blend
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern1candle
{
	cull none
	{
		map models/mapobjects/lights/lantern1candle
		depthwrite
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern2
{
	cull none
	{
		map models/mapobjects/lights/lantern2
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern2candle
{
	cull none
	{
		map models/mapobjects/lights/lantern2candle
		depthwrite
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern2glass
{
	cull none
	surfaceparm trans
	surfaceparm lightfilter
	{
		map models/mapobjects/lights/lantern2glass
		alphagen const 0.2
		blendfunc blend
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern3
{
	cull none
	{
		map models/mapobjects/lights/lantern3
		rgbgen vertex
	}
}

models/mapobjects/lights/lantern3candle
{
	cull none
	{
		map models/mapobjects/lights/lantern3candle
		depthwrite
		rgbgen vertex
	}
}

models/mapobjects/lights/torch
{
	surfaceparm detail
	{
		map models/mapobjects/lights/torch
		rgbgen vertex
	}
}

models/mapobjects/lights/torchholder
{
	surfaceparm detail
	{
		map models/mapobjects/lights/torchholder
		rgbgen vertex
	}
}

models/mapobjects/lights/flame
{
	cull none
	deformvertexes autosprite
	surfaceparm trans
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm nomarks
	surfaceparm nonsolid
	deformvertexes move -0.347877 0.003964 0.157837 sin 0 1 1.836573 1.038147
	deformvertexes wave 10 sin 0 0.5 1 3.5
	q3map_surfacelight 200
	q3map_lightimage models/mapobjects/lights/flame
	sort banner
	{
		clampmap models/mapobjects/lights/flame
		rgbgen identity
		blendfunc blend
		depthwrite
		alphafunc GT0
	}
}

models/mapobjects/lights/candleflame
{
	cull none
	deformvertexes autosprite
	surfaceparm trans
	surfaceparm nolightmap
	surfaceparm nodlight
	surfaceparm nomarks
	surfaceparm nonsolid
	deformvertexes move -0.347877 0.003964 0.157837 sin 0 1 1.836573 1.038147
	deformvertexes wave 10 sin 0 0.5 1 3.5
	q3map_surfacelight 600
	q3map_lightimage models/mapobjects/lights/flame
	sort banner
	{
		clampmap models/mapobjects/lights/flame
		rgbgen identity
		blendfunc blend
		depthwrite
		alphafunc GT0
	}
}

models/mapobjects/lights/flamebounds
{
	surfaceparm nodraw
	surfaceparm trans
	qer_trans 0.4
	{
		rgbgen identity
	}
}

//
// weapons
//

// bolt
models/weapons2/crossbow/bolt
{
	cull none
	surfaceparm trans
	{
		clampmap models/weapons2/crossbow/bolt
		rgbgen vertex
		blendfunc blend
		depthwrite
		alphafunc GT0
	}
}
