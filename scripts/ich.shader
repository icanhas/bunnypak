textures/test/sunsky
{
	q3map_sun 1 1 .84 40 -54.65 42
	q3map_surfacelight 400
	surfaceparm noimpact
	surfaceparm nolightmap
	surfaceparm sky
	{
		map textures/test/sunsky
	}
}

textures/test/grassblend
{
	qer_editorimage textures/test/grass.png
	q3map_tcGen ivector ( 128 0 0 ) ( 0 128 0 )
	{
		map textures/test/grass
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
}

textures/test/dirtblend
{
	qer_editorimage textures/test/dirt.png
	q3map_tcGen ivector ( 128 0 0 ) ( 0 128 0 )
	{
		map textures/test/dirt
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
}

textures/test/dirtgrass
{
	qer_editorimage textures/test/grass
	q3map_tcGen ivector ( 128 0 0 ) ( 0 128 0 )
	{
		map textures/test/dirt
		rgbGen identity
	}
	{
		map textures/test/grass
		blendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA
		alphaGen vertex
		rgbGen identity
	}
	{
		map $lightmap
		blendFunc GL_DST_COLOR GL_ZERO
		rgbGen identity
	}
}

textures/common/alpha0
{
	qer_editorimage textures/common/alpha0
	q3map_alphaMod volume
	q3map_alphaMod set 0.0
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
	qer_trans 0.7
}

textures/common/alpha100
{
	qer_editorimage textures/common/alpha100
	q3map_alphaMod volume
	q3map_alphaMod set 1.0
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
	qer_trans 0.7
}


textures/common_alphascale/alpha_100
{
	qer_trans 0.5
	q3map_alphaMod volume
	q3map_alphaMod scale 1.0
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common_alphascale/alpha_75
{
	qer_trans 0.5
	q3map_alphaMod volume
	q3map_alphaMod scale 0.75
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common_alphascale/alpha_50
{
	qer_trans 0.5
	q3map_alphaMod volume
	q3map_alphaMod scale 0.50
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common_alphascale/alpha_25
{
	qer_trans 0.5
	q3map_alphaMod volume
	q3map_alphaMod scale 0.25
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}

textures/common_alphascale/alpha_0
{
	qer_trans 0.5
	q3map_alphaMod volume
	q3map_alphaMod scale 0
	surfaceparm nodraw
	surfaceparm nonsolid
	surfaceparm trans
}
